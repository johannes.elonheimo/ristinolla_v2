package asetukset;

import pelilogiikka.RistinollaPeli;

import java.io.File;

public class Asetukset {
    /*
    String atribuutteja joita hyödynnetään asetustiedostojen oikeanlaisessa lukemisessa ja tallentamisessa.
     */
    protected final String pelaaja1 = "player1", pelaaja2 ="player2";
    protected final String pelaaja1merkki = "player1mark", pelaaja2merkki ="player2mark";
    protected final String pelaaja1pisteet = "player1points", pelaaja2pisteet = "player2points";
    protected final String ruudukonKoko = "gridSize";
    /*
    File-olio, joka luodaan aliluokkien BufferedReaderia ja -Writeria varten;
     */
    protected final File file;
    /*
    Ristinollapeli-olio, jotta aliluokkien konstruktorit voivat
    alustaa sen kutsumalla yliluokan konstruktoria.
     */
    protected RistinollaPeli peli;

    public Asetukset(RistinollaPeli peli,String tiedostoNimi) {
        this.peli = peli;
        this.file = new File(tiedostoNimi);
    }

}
