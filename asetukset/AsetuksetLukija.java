package asetukset;

import pelilogiikka.RistinollaPeli;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AsetuksetLukija extends Asetukset {
    /*
    AsetuksetLukija-oliota käytetään lukemaan seuraavalla formaatilla nimettyjä tekstitiedostoja (.txt).
    Formaatti:
    Ristinolla Asetukset <ennalta määritetty SimpleDateFormat-ajanilmaisu>.txt
     */
    public AsetuksetLukija(RistinollaPeli peli, String tiedostoNimi) {
        super(peli, tiedostoNimi);
    }
    /*
    Metodi lukee konstruktorissa välitetyn nimen mukaisen tekstitiedoston sisältöä.
    Tiedostosta luetun tiedon avulla asetetaan pelin molempiin pelaaja-olioihin uudet tiedot. (nimi, merkki, pisteet)
    Myös RistinollaPeli-olion ruudukon koko voidaan määritellä uudelleen.
     */
    public void lataaAsetukset() {
        BufferedReader reader;
        try{
            System.out.println("Loading settings");
            reader = new BufferedReader(new FileReader(super.file));

            /*
            Luetaan BufferedReaderin avulla seuraava rivi tekstiedostosta.
            Rivistä poistetaan teksti, jota ei ole tarkoitus asettaa RistinollaPelin pelaaja-oliolle.
             */
            peli.getPelaaja1().setNimi(reader.readLine().replace(super.pelaaja1+":",""));

            peli.getPelaaja2().setNimi(reader.readLine().replace(super.pelaaja2+":",""));

            peli.getPelaaja1().setMerkki(reader.readLine().replace(super.pelaaja1merkki+":",""));

            peli.getPelaaja2().setMerkki(reader.readLine().replace(super.pelaaja2merkki+":",""));

            int pisteet1 = Integer.parseInt(reader.readLine().replace(super.pelaaja1pisteet+":",""));
            peli.getPelaaja1().setPisteet(pisteet1);

            int pisteet2 = Integer.parseInt(reader.readLine().replace(super.pelaaja2pisteet+":",""));
            peli.getPelaaja2().setPisteet(pisteet2);

            int gridSize = Integer.parseInt(reader.readLine().replace(super.ruudukonKoko+":",""));
            peli.setKoko(gridSize);


            String line;
            while(( line= reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
        }
        catch(IOException e) {
            System.out.println("Lataaminen epäonnistui");
        }

    }

}
