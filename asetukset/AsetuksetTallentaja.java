package asetukset;

import pelilogiikka.RistinollaPeli;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AsetuksetTallentaja extends Asetukset {
    public AsetuksetTallentaja(RistinollaPeli peli) {
        /*
        new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date())
        Lisää konstruktorissa annettavaan String-olioon sekunnilleen tarkan ajan.
        (VVVV-KK-PP TT-MM-SS)
         */
        super(peli,
                "Ristinolla Asetukset "+
                        new SimpleDateFormat("yyyy-MM-dd HH-mm-ss")
                                .format(new Date()) +".txt");
    }
    public void tallennaAsetukset(){
        BufferedWriter writer;
        try {
            System.out.println("Tallennetaan asetuksia");
            writer = new BufferedWriter(new FileWriter(super.file, true));
            System.out.println(super.file.getCanonicalPath());
            /*
            Asetuksia kirjoitetaan rivi riviltä syöttäen peliin liittyviä olennaisia arvoja.
            Peliruudukon tilaa ei kuitenkaan tallenneta.
             */
            writer.write(super.pelaaja1+":"+ peli.getPelaaja1().getNimi());
            writer.newLine();
            writer.write(super.pelaaja2+":"+ peli.getPelaaja2().getNimi());
            writer.newLine();

            writer.write(super.pelaaja1merkki+":"+ peli.getPelaaja1().getMerkki());
            writer.newLine();
            writer.write(super.pelaaja2merkki+":"+ peli.getPelaaja2().getMerkki());
            writer.newLine();

            writer.write(super.pelaaja1pisteet+":"+ peli.getPelaaja1().getPisteet());
            writer.newLine();
            writer.write(super.pelaaja2pisteet+":"+ peli.getPelaaja2().getPisteet());
            writer.newLine();

            writer.write(super.ruudukonKoko+":"+ peli.getKoko());
            writer.newLine();

            writer.close();
        }
        catch(IOException e) {
            System.out.println("Saving settings failed");
        }
    }


}
