package pelilogiikka;

public class Pelaaja {
    private String nimi;
    private String merkki;
    private int pisteet;

    public Pelaaja(String nimi, String merkki, int pisteet) {
        this.nimi = nimi;
        this.merkki = merkki;
        this.pisteet = pisteet;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getMerkki() {
        return merkki;
    }

    public void setMerkki(String merkki) {
        this.merkki = merkki;
    }

    public int getPisteet() {
        return pisteet;
    }

    public void setPisteet(int pisteet) {
        this.pisteet = pisteet;
    }
    /*
    Lisätty, jotta yhden pisteen antaminen pelaajalle olisi helpompaa.
     */
    public void annaPiste() {
        this.pisteet++;
    }
}
