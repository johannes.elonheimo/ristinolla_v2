package pelilogiikka;

public class RistinollaPeli {

    private String[][] ruudukko;//ruudukko joka sisältää pelitason merkit

    private int koko; //ruudukon kanta & korkeus

    //true == pelaaja1 pelaajan1Vuoro    false == pelaaja2 pelaajan1Vuoro
    private boolean pelaajan1Vuoro = true;

    private Pelaaja pelaaja1, pelaaja2;

    public RistinollaPeli(Pelaaja pelaaja1, Pelaaja pelaaja2, int koko) {
        this.pelaaja1 = pelaaja1;
        this.pelaaja2 = pelaaja2;
        this.koko = koko;
        ruudukko = new String[this.koko][this.koko];
    }
    //alustaa uuden ruudukon
    public void uusiRuudukko() {
        ruudukko = new String[this.koko][this.koko];
    }

    public Pelaaja getPelaaja1() {
        return pelaaja1;
    }
    public void setPelaaja1(Pelaaja pelaaja1) {
        this.pelaaja1 = pelaaja1;
    }

    public Pelaaja getPelaaja2() {
        return pelaaja2;
    }
    public void setPelaaja2(Pelaaja pelaaja2) {
        this.pelaaja2 = pelaaja2;
    }

    public int getKoko() {
        return koko;
    }
    public void setKoko(int koko) {
        this.koko = koko;
    }
    //palauttaa tämän vuoron pelaajan pelaajan1Vuoro booleanin avulla.
    public Pelaaja annaTämänVuoronPelaaja() {
        if (pelaajan1Vuoro) {
            return pelaaja1;
        } else {
            return pelaaja2;
        }
    }
    public void vaihdaVuoro() {
        pelaajan1Vuoro = !pelaajan1Vuoro;
    }
    /*
    käy ruudukon läpi ja tarkastaa onko voitto
     */
    public boolean tarkistaVoitto() {
        String aikaisempiString = null;
        for (int i = 0; i<this.koko;i++) {
            /*
            rivit
             */
            aikaisempiString = null;
            for (int ii = 0; ii<this.koko;ii++) {
                String current = ruudukko[i][ii];
                if (stringVertailu(aikaisempiString,current)) {
                    aikaisempiString = current;
                } else break;
                if (ii==this.koko-1) {
                    return true;
                }
            }
            /*
            sarakkeet
             */
            aikaisempiString = null;
            for (int ii = 0; ii<this.koko;ii++) {
                String current = ruudukko[ii][i];
                if (stringVertailu(aikaisempiString,current)) {
                    aikaisempiString = current;
                } else break;
                if (ii==this.koko-1) {
                    return true;
                }
            }
        }
        //from down left to up right (x ja y molemmat nousee yhdellä)
        aikaisempiString = null;
        for (int i = 0; i<this.koko;i++) {
            String current = ruudukko[i][i];
            if (stringVertailu(aikaisempiString,current)) {
                aikaisempiString = current;
            } else break;
            if (i==this.koko-1) {
                return true;
            }
        }
        //from up left to down right (y laskee yhdellä ja x nousee)
        aikaisempiString = null;
        for (int i = 0; i<this.koko;i++) {

            String current = ruudukko[i][this.koko-i-1];
            if (stringVertailu(aikaisempiString,current)) {
                aikaisempiString = current;
            } else break;
            if (i==this.koko-1) {
                return true;
            }

        }
        return false;
    }
    public boolean tarkistaTasapeli() {
        for (int i = 0; i < this.koko; i++) {
            for (int ii = 0; ii < this.koko; ii++) {
                if (ruudukko[i][ii] == null) {
                    return false;
                }
            }
        }
        return true;
    }
    //asettaa pelaajan merkin ruudukon koordinaatteihin
    public void asetaMerkki(Pelaaja pelaaja, int x, int y) {
        ruudukko[x][y] = pelaaja.getMerkki();
    }
    /*
    Vertaa aikaisempaa ja nykyistä string arvoa.
    jos aikaisempi on null palauttaa truen
    jos aikaisempi ja nykyinen on sama arvo palauttaa true
    jos nykyinen on null palauttaa false
     */
    private boolean stringVertailu(String previous, String current) {
        if (current != null) {
            if (previous == null ) {
                return true; //palauttaa true koska on silloin uudella rivillä
            }
            if (previous.compareTo(current) != 0){
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
