package grafiikka;

import asetukset.AsetuksetLukija;
import asetukset.AsetuksetTallentaja;
import pelilogiikka.RistinollaPeli;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class MenuPalkki extends JMenuBar {
    public MenuPalkki(RistinollaPeli peli,RistinollaPaneeli ristinollaPaneeli) {
        JMenu settings = new JMenu("Asetukset");
        JMenuItem settingsOpenWindow = new JMenuItem("Avaa asetuset-ikkuna");
        settingsOpenWindow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AsetuksetIkkuna(peli,ristinollaPaneeli);
            }
        });
        settings.add(settingsOpenWindow);
        this.add(settings);

        JMenu prevGame = new JMenu("Aikaisemmat pelit");
        /*
        Lisää JMenuun JItemeitä, joiden niminä on kaikki ne tekstitiedostot,
        jotka se löytää suoritettavan java-tiedoston sijainnista,
        joiden nimessä on Ristinolla Asetukset.
         */
        for (String s : getAsetusTiedostoNimet()) {
            JMenuItem previousGameMenuItem = new JMenuItem(s);
            previousGameMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AsetuksetLukija asetuksetLukija = new AsetuksetLukija(peli,s);
                    asetuksetLukija.lataaAsetukset();
                    peli.uusiRuudukko();
                    ristinollaPaneeli.uusiRuudukko();
                    ristinollaPaneeli.sivunAlkuPaneeli.päivitä();
                }
            });
            prevGame.add(previousGameMenuItem);
        }
        this.add(prevGame);

        JMenu saveCurrentGame = new JMenu( "Pelin tallennus");
        JMenuItem saveCurrentGameItem = new JMenuItem("Tallenna peli");
        saveCurrentGameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AsetuksetTallentaja asetuksetTallentaja = new AsetuksetTallentaja(peli);
                asetuksetTallentaja.tallennaAsetukset();
                JOptionPane.showMessageDialog(
                        null,
                        "Tallennus onnistui",
                        "Tallentaminen.",
                        JOptionPane.WARNING_MESSAGE); // Aukaisee ikkunan, joka kertoo, jos pelin tallentaminen onnistui.
            }
        });
        saveCurrentGame.add(saveCurrentGameItem);
        this.add(saveCurrentGame);
    }
    /*
    Etsii tietokoneen tiedostorakenteesta ristinollasetukset tekstitiedostoja suhteessa suoritettavan java tiedoston sijaintiin.
     Palauttaa vain ristinolla asetuksien nimeämiskäytäntöjä noudattavat tiedostonimet.
     */
    private String[] getAsetusTiedostoNimet() {
        ArrayList<String> fileNames = new ArrayList<>();
        File[] filesList = new File(".").listFiles();
        for(File f : filesList){
            if(f.isFile() && f.getName().contains("Ristinolla Asetukset")){
                fileNames.add(f.getName());
            }
        }
        return fileNames.toArray(new String[] {});
    }
}
