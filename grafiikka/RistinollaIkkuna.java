package grafiikka;

import pelilogiikka.Pelaaja;
import pelilogiikka.RistinollaPeli;

import javax.swing.*;
import java.awt.*;

public class RistinollaIkkuna extends JFrame {
    private JPanel superPanel = new JPanel(new BorderLayout());
    // alustetaan pelin logiikka oletusasetuksilla
    private RistinollaPeli peli = new RistinollaPeli(
            new Pelaaja("Pelaaja 1","x",0),
            new Pelaaja("Pelaaja 2","o",0),
            3 /*oletus pelin koko 3x3*/
            );
    /*
    Alustetaan peliin lisättävät grafiikkapaneelit ja menut oikeassa järjestyksessä,
    jotta niiden konstruktoreihin pystytään viittaamaan tarvittavia grafiikkaan tai
    pelilogiikkaan liittyviä olioita.
     */
    private SivunAlkuPaneeli sivunAlkuPaneeli = new SivunAlkuPaneeli(this.peli);
    RistinollaPaneeli ristinollaPaneeli= new RistinollaPaneeli(peli,sivunAlkuPaneeli);
    private JMenuBar menuPalkki = new MenuPalkki(peli,ristinollaPaneeli);

    public RistinollaIkkuna() throws HeadlessException {
        this.setJMenuBar(menuPalkki); //menupalkki lisätään suoraan JFrame-olioon.
        setTitle("Ristinolla by Johannes Elonheimo, Iiro Paakkonen ja Aarni Miikkulainen");
        /*
        Muut grafiikka-oliot lisätään luokan alussa alustettuun superpaneliin Borderlayouttiin muotoillen.
         */
        superPanel.add(sivunAlkuPaneeli,BorderLayout.PAGE_START);
        superPanel.add(ristinollaPaneeli,BorderLayout.CENTER);
        // superPanel, johon on lisätty muut grafiikka-oliot lisätään JFrameen
        this.add(superPanel);
        setSize(300*peli.getKoko(), 300*peli.getKoko());
        setPreferredSize(getSize());
        setLocationRelativeTo(null); //alustaa JFramen keskelle näyttöä
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE); //sulkee ohjelman painettaessa X-nappia ylänurkassa.
    }
}
