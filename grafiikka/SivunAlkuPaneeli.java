package grafiikka;

import pelilogiikka.RistinollaPeli;

import javax.swing.*;
import java.awt.*;

public class SivunAlkuPaneeli extends JPanel {
    RistinollaPeli peli;
    public SivunAlkuPaneeli(RistinollaPeli peli) {
        this.setLayout(new GridLayout(1,3));
        this.peli = peli;
        päivitä(); //käytetään päivitys metodia Paneelin alustamiseen
    }
    public void päivitä() {
        this.removeAll(); // poistetaan mahdolliset vanhat komponenti pois paneelista
        //Pelaajan 1 nimi ja merkki paneelin vasempaan reunaan
        JLabel merkkiNimiPelaaja1 = new JLabel(peli.getPelaaja1().getNimi()+" : "+ peli.getPelaaja1().getMerkki());
        //Paneelin keskelle pelaajien pisteet
        JLabel pisteet = new JLabel(peli.getPelaaja1().getPisteet()+" : "+ peli.getPelaaja2().getPisteet());
        pisteet.setHorizontalAlignment(SwingConstants.CENTER);
        //Pelaajan 2 nimi ja merkki paneelin vasempaan reunaan
        JLabel merkkiNimiPelaaja2 = new JLabel(peli.getPelaaja2().getNimi()+" : "+ peli.getPelaaja2().getMerkki());
        merkkiNimiPelaaja2.setHorizontalAlignment(SwingConstants.RIGHT);
        //Korostetaan pelaajan JLabel-olio samalla värillä, mikä on pelaajan graafisen ristinollaruudukon ruuduissa.
        if (merkkiNimiPelaaja1.getText().contains(peli.annaTämänVuoronPelaaja().getNimi())) {
            merkkiNimiPelaaja1.setForeground(Color.decode("#7BE0AD"));
        } else {
            merkkiNimiPelaaja2.setForeground(Color.decode("#FCB5B5"));
        }

        this.add(merkkiNimiPelaaja1);
        this.add(pisteet);
        this.add(merkkiNimiPelaaja2);
        this.updateUI(); //päivittää grafiikat

    }
}
