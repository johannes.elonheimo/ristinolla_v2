package grafiikka;

import pelilogiikka.Pelaaja;
import pelilogiikka.RistinollaPeli;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AsetuksetIkkuna extends JFrame{
    private final String PELAAJA_1_JLABEL = "1. pelaajan nimi";
    private final String PELAAJA_2_JLABEL = "2. pelaajan nimi";
    private final String PELAAJA_1_MERKKI = "1. pelaajan merkit (1-3 merkkiä)";
    private final String PELAAJA_2_MERKKI = "2. pelaajan merkit (1-3 merkkiä)";
    private final String GRIDSIZE_TEXT = "Ruudukon kanta & korkeus. Pariton luku, joka on suurempi kuin 1";
    private final int width = 750, height =400;
    public AsetuksetIkkuna(RistinollaPeli peli, RistinollaPaneeli ristinollaPaneeli) {
        setTitle("Settings");


        this.setLayout(new GridLayout(3,2));

        /*
        Rivi 1 Sarake 1 komponenttien luonti ja lisäys
         */
        JPanel jPanelx1y1 = new JPanel();

        JLabel pelaaja1Jlabel = new JLabel(PELAAJA_1_JLABEL);
        jPanelx1y1.add(pelaaja1Jlabel);

        JTextField pelaaja1TextField = new JTextField("");
        jPanelx1y1.add(pelaaja1TextField);

        jPanelx1y1.setLayout(new GridLayout(2,1));
        this.add(jPanelx1y1);
        /*
        Rivi 1 Sarake 2
         */
        JPanel jPanelx2y1 = new JPanel();

        JLabel pelaaja2Jlabel = new JLabel(PELAAJA_2_JLABEL);
        jPanelx2y1.add(pelaaja2Jlabel);

        JTextField pelaaja2TextField = new JTextField("");
        jPanelx2y1.add(pelaaja2TextField);

        jPanelx2y1.setLayout(new GridLayout(2,1));
        this.add(jPanelx2y1);
        /*
        Rivi 2 Sarake 1 pelaajan 1 merkin asetus
         */
        JPanel jPanelx1y2 = new JPanel();

        JLabel pelaaja1MerkkiJlabel = new JLabel(PELAAJA_1_MERKKI);
        jPanelx1y2.add(pelaaja1MerkkiJlabel);

        JTextField pelaaja1MerkkiTextField = new JTextField("");
        jPanelx1y2.add(pelaaja1MerkkiTextField);

        jPanelx1y2.setLayout(new GridLayout(2,1));
        this.add(jPanelx1y2);



        /*
        Rivi 2 Sarake 2
         */

        JPanel jPanelx2y2 = new JPanel();

        JLabel pelaaja2MerkkiJlabel = new JLabel(PELAAJA_2_MERKKI);
        jPanelx2y2.add(pelaaja2MerkkiJlabel);

        JTextField pelaaja2MerkkiTextField = new JTextField(10);
        jPanelx2y2.add(pelaaja2MerkkiTextField);

        jPanelx2y2.setLayout(new GridLayout(2,1));
        this.add(jPanelx2y2);

        /*
        Rivi 3 Sarake 1
         */

        JPanel jPanelx1y3 = new JPanel();

        JLabel gridSizeJlabel = new JLabel(GRIDSIZE_TEXT);
        jPanelx1y3.add(gridSizeJlabel);

        JTextField gridSizeTextField = new JTextField("");
        jPanelx1y3.add(gridSizeTextField);

        jPanelx1y3.setLayout(new GridLayout(2,1));
        this.add(jPanelx1y3);

        /*
        Rivi 3 Sarake 2
         */
        JButton startGame = new JButton("Aloita");
        startGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean properInput = true;
                /*
                tarkistukset
                 */

                //pelaajan nimen pituus <= 16 merkkiä
                if ((pelaaja1TextField.getText().length() > 16 || pelaaja2TextField.getText().length() > 16 ) ||
                        (pelaaja1TextField.getText().length() ==0 || pelaaja2TextField.getText().length() ==0 )) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Nimen pituuden on oltava suurempi kuin 0 \n ja maksimissaan 16 merkkiä",
                            "Virheellinen nimen pituus",
                            JOptionPane.WARNING_MESSAGE);
                    properInput = false;
                }

                //pelaajan merkin pituus <= 3 merkkiä
                if ((pelaaja1MerkkiTextField.getText().length() > 3 || pelaaja2MerkkiTextField.getText().length() > 3 ) ||
                        (pelaaja1MerkkiTextField.getText().length() ==0 || pelaaja2MerkkiTextField.getText().length() ==0 )) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Merkin pituuden on oltava suurempi kuin 0 \n ja maksimissaan 3 merkkiä",
                            "Virheellinen pelaajan merkin pituus",
                            JOptionPane.WARNING_MESSAGE);
                    properInput = false;
                }
                //dimension ristinolla<=3  && ristinolla % 2 =1
                if (gridSizeTextField.getText().matches("\\d+")) { //tarkistaa onko luku
                    int gridSize = Integer.parseInt(gridSizeTextField.getText());
                    if (gridSize < 3 || gridSize % 2 ==0) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Syötteen on oltava pariton luku, joka on suurempi kuin 1",
                                "Virheellinen kanta & korkeus -syöte",
                                JOptionPane.WARNING_MESSAGE);
                        properInput = false;
                    }
                } else {
                    JOptionPane.showMessageDialog(
                            null,
                            "Syötä pelkästään numeroita ja syöte ei saa olla tyhjä.",
                            "Virheellinen kanta & korkeus -syöte",
                            JOptionPane.WARNING_MESSAGE);
                    properInput = false;
                }
                /*
                datan asettaminen
                 */
                if (properInput) {
                    peli.setPelaaja1(new Pelaaja(pelaaja1TextField.getText(),pelaaja1MerkkiTextField.getText(),0));
                    peli.setPelaaja2(new Pelaaja(pelaaja2TextField.getText(),pelaaja2MerkkiTextField.getText(),0));
                    peli.setKoko(Integer.parseInt(gridSizeTextField.getText()));
                    peli.uusiRuudukko();
                    ristinollaPaneeli.uusiRuudukko();
                    dispose();
                }
                //

            }
        });
        this.add(startGame);



        setSize(width, height);

        setPreferredSize(getSize());
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
