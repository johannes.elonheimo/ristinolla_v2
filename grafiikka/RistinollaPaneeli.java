package grafiikka;

import pelilogiikka.RistinollaPeli;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RistinollaPaneeli extends JPanel {
    RistinollaPeli peli;
    SivunAlkuPaneeli sivunAlkuPaneeli;
    public RistinollaPaneeli(RistinollaPeli peli, SivunAlkuPaneeli sivunAlkuPaneeli) {
        this.peli = peli;
        this.sivunAlkuPaneeli = sivunAlkuPaneeli;
        uusiRuudukko();

    }
    /*
    Lisää paneeliin varsinaisen graafisen ristinollaruudukon.
    Jokainen ruutu on JButton.
     */
    protected void uusiRuudukko() {
        this.setLayout(new GridLayout(peli.getKoko(),peli.getKoko()));
        this.removeAll();
        for (int i = 0; i< peli.getKoko() ; i++) {
            for(int ii = 0; ii< peli.getKoko(); ii++){
                JButton peliNappi = new JButton();
                peliNappi.setBackground(Color.WHITE);
                peliNappi.addActionListener(
                        new NappiToimintoKuuntelija(i,ii,peli,peliNappi,this,sivunAlkuPaneeli)
                ); // Lisätään luokan lopussa määritelty NappiToimintoKuuntelija JButton olioon
                this.add(peliNappi); //lisää napit jpaneliin
                this.updateUI();
            }
        }
    }
}
class NappiToimintoKuuntelija implements ActionListener {
    private int x,y;
    private RistinollaPeli peli;
    private JButton nappi;
    private RistinollaPaneeli ruudukko;
    private SivunAlkuPaneeli sivunAlkuPaneeli;
    public NappiToimintoKuuntelija(int x, int y, RistinollaPeli peli,JButton nappi, RistinollaPaneeli ruudukko, SivunAlkuPaneeli sivunAlkuPaneeli) {
        this.x = x;
        this.y = y;
        this.peli = peli;
        this.nappi = nappi;
        this.ruudukko = ruudukko;
        this.sivunAlkuPaneeli = sivunAlkuPaneeli;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!nappi.getText().equals("")){
            return; //lopetataan metodin suoritus, jos nappia on jo painettu
        } else {
            nappi.setText(peli.annaTämänVuoronPelaaja().getMerkki()); //asetetaan nappiin nappia painaneen pelaajan merkki
            nappi.setFont(new Font("Impact",Font.PLAIN,200/peli.getKoko())); //asetetaan fontti, jonka koko mukautuu ruudukon kokoon.
            peli.asetaMerkki(peli.annaTämänVuoronPelaaja(),x,y); //asetetaan RistinollaPeli-olioon pelaajan merkki oiekaan ruutuun.
            if (peli.tarkistaVoitto()) { // jos pelaaja voitti sen jälkeen kun laittoi merkkinsä ruudukolle
                System.out.println(peli.annaTämänVuoronPelaaja().getNimi()+ " voitti");
                peli.annaTämänVuoronPelaaja().annaPiste();
                peli.uusiRuudukko(); //uusitaan pelilogiikan ruudukko
                ruudukko.uusiRuudukko(); // uusitaan graafinen ruudukko
                JOptionPane.showMessageDialog(
                        null,
                        (peli.annaTämänVuoronPelaaja().getNimi()+ " on voittaja"),"Voitto kotiin!",
                        JOptionPane.PLAIN_MESSAGE); //annetaan pelaajille ilmoitusikkuna
            } else if (peli.tarkistaTasapeli()){
                peli.uusiRuudukko();
                ruudukko.uusiRuudukko();
                JOptionPane.showMessageDialog(
                        null,
                        "Tasapeli",
                        "Tasapeli",
                        JOptionPane.PLAIN_MESSAGE);
            }
            vainhdaNapinVäri(nappi); // vaihtaa napin värin painalluksen jälkeen
            peli.vaihdaVuoro(); //antaa vuron toiselle pelaajalle
            sivunAlkuPaneeli.päivitä(); //päivittää yläpaneelin grafiikat, koska vuoro on vaihtunut ja pisteet mahdollisesti muuttuneet.
        }

    }
    private void vainhdaNapinVäri(JButton button) {
        if (peli.annaTämänVuoronPelaaja() == peli.getPelaaja1()) {
            button.setBackground(Color.decode("#7BE0AD"));
        } else {
            button.setBackground(Color.decode("#FCB5B5"));
        }
    }
}
