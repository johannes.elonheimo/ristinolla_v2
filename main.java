import grafiikka.RistinollaIkkuna;

public class main {
    public static void main(String[] args) {
        if (args.length>0 && args[0].length() >0) {
            //ensimmäisen parametrin numero määrittää kuinka monta ristinollaikkunaa avataan
            for (int i = 0; i < Integer.parseInt(args[0]); i++) {
                new RistinollaIkkuna();
            }
        } else {
            new RistinollaIkkuna();
        }


    }
}
